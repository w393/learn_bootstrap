module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160058/learn_bootstrap/'
    : '/'
}
